# Battleships - Guestline Recruitment

## Overview

This project wasn't just to showcase my programming skills, but I wanted to demonstrate through it what my daily work environment looks like and what things beyond the code are important to me.

That's why I decided on a Bitbucket repository integrated with Jira. At the very beginning of the project I created most of the issues on Jira with descriptions of what they are about. Work on each ticket was done on the feature branch, from which I made a PR, which at the end was merged to develop. This may seem pointless in a one-person project, but this is to show that such a workflow with short-lived branches is important to me and I'm well accustomed to it. I've also configured a CI tool, which was running the build on each activity on the repository.

I've set Jira and CircleCI availability as public, so I hope you have some insight into them.

## Technologies used

- Java 11
- JavaFX 11
- Maven
- CircleCI

## How to run

- #### Client distribution
  The easiest way to run the application will be to use the distribution I have put on the repository.
  You don't need anything more than GIT installed and the ability to unzip a ZIP. You don't even need to have Java on your computer, as I have provided it in the distribution.

Clone repository

```bash
  git clone https://Ortench@bitbucket.org/team-1594986160121/battleships.git
```

Unzip the ZIP file

```bash
  client-dist-1.0.0-SNAPSHOT.zip
```

Run file

```bash
  start.bat
```

If for some reason you would like to build the project yourself and generate the distribution then you need Maven and to call the following command in the root directory of the project

```bash
  mvn clean install -Pdist
```

You will find the distribution in the target directory of the client-dist module.

- #### Start from IDE
  If you want to run the project from the IDE, start by building it with Maven before opening it in IDE.

```bash
  mvn clean install
```

The starting class is

```bash
  BattleStart
```

I know that sometimes people have trouble running a JaveFX-based application in InteliJ since FX stopped being part of the JDK.
If you encounter any errors then a possible solution would be to download JavaFX SDK from
[here](https://gluonhq.com/products/javafx/) and add the following command to the VM options:

```bash
  --module-path path_to_javafx_sdk\lib --add-modules=javafx.controls,javafx.fxml
```
