package pl.jprzybyla.placement;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.jprzybyla.board.Point;
import pl.jprzybyla.ships.Ship;
import pl.jprzybyla.ships.ShipFactory;
import pl.jprzybyla.ships.ShipType;

import java.security.SecureRandom;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;
import static pl.jprzybyla.board.BoardManager.getPoint;

class PlacementStrategyTest
{
	private Ship ship;
	private HorizontalPlacementStrategy horizontalPlacementStrategy;
	private VerticalPlacementStrategy verticalPlacementStrategy;

	@BeforeEach
	void init()
	{
		ship = ShipFactory.create( ShipType.TEST_SHIP_WITH_SIZE_3 );
		SecureRandom rand = mock( SecureRandom.class );
		when( rand.nextInt( anyInt() ) ).thenReturn( 4 );
		horizontalPlacementStrategy = new HorizontalPlacementStrategy( rand );
		verticalPlacementStrategy = new VerticalPlacementStrategy( rand );

	}

	@Test
	void shouldCreateCorrectDeploymentLineForHorizontalStrategy()
	{
		//when
		List<Point> deploymentLine = horizontalPlacementStrategy.createDeploymentLine( ship.getSize() );

		//then
		assertEquals( 3, deploymentLine.size() );
		assertEquals( getPoint( 4, 4 ), deploymentLine.get( 0 ) );
		assertEquals( getPoint( 5, 4 ), deploymentLine.get( 1 ) );
		assertEquals( getPoint( 6, 4 ), deploymentLine.get( 2 ) );
	}

	@Test
	void shouldCreateCorrectDeploymentLineForVerticalStrategy()
	{
		//when
		List<Point> deploymentLine = verticalPlacementStrategy.createDeploymentLine( ship.getSize() );

		//then
		assertEquals( 3, deploymentLine.size() );
		assertEquals( getPoint( 4, 4 ), deploymentLine.get( 0 ) );
		assertEquals( getPoint( 4, 5 ), deploymentLine.get( 1 ) );
		assertEquals( getPoint( 4, 6 ), deploymentLine.get( 2 ) );
	}

	@Test
	void shouldCalculateStartPointCorrectly()
	{
		//given
		AbstractPlacementStrategy vertPlacementStrategy = new VerticalPlacementStrategy();
		AbstractPlacementStrategy horPlacementStrategy = new HorizontalPlacementStrategy();

		//when
		List<Point> vertDeploymentLine = vertPlacementStrategy.createDeploymentLine( ship.getSize() );
		List<Point> horDeploymentLine = horPlacementStrategy.createDeploymentLine( ship.getSize() );

		//then
		assertTrue( vertDeploymentLine.get( 0 )
		                          .getY() >= 0 && vertDeploymentLine.get( 0 )
		                                                        .getY() <= 7 );
		assertTrue( vertDeploymentLine.get( 0 )
		                          .getX() >= 0 && vertDeploymentLine.get( 0 )
		                                                        .getX() <= 9 );

		assertTrue( horDeploymentLine.get( 0 )
		                              .getY() >= 0 && horDeploymentLine.get( 0 )
		                                                                .getY() <= 9 );
		assertTrue( horDeploymentLine.get( 0 )
		                              .getX() >= 0 && horDeploymentLine.get( 0 )
		                                                                .getX() <= 7 );
	}
}