package pl.jprzybyla.board;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.jprzybyla.placement.PlacementType;
import pl.jprzybyla.ships.Ship;
import pl.jprzybyla.ships.ShipFactory;
import pl.jprzybyla.ships.ShipType;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BoardManagerTest
{
	private BoardManager boardManager;
	private Ship shipWithSize1;
	private Ship shipWithSize3;

	@BeforeEach
	void init()
	{
		boardManager = new BoardManager();
		shipWithSize1 = ShipFactory.create( ShipType.TEST_SHIP );
		shipWithSize3 = ShipFactory.create( ShipType.TEST_SHIP_WITH_SIZE_3 );
	}

	private void testPutOnBoard( Ship aShip, List<Point> deploymentPoints )
	{
		boardManager.putOnBoard( deploymentPoints, aShip, PlacementType.HORIZONTAL );
	}

	@Test
	void shouldAddShip()
	{
		//when
		testPutOnBoard( shipWithSize1, new ArrayList<>( List.of( new Point( 0, 0 ) ) ) );

		Ship shipFromBoard = boardManager.getShipByPoint( BoardManager.getPoint( 0, 0 ) );

		//then
		assertEquals( shipWithSize1, shipFromBoard );
	}

	@Test
	void shouldReturnNullWhenFiledIsEmpty()
	{
		//when
		Ship shipFromBoard = boardManager.getShipByPoint( BoardManager.getPoint( 0, 0 ) );

		//then
		assertNull( shipFromBoard );
	}

	@Test
	void shouldThrowExceptionWenDeploymentLineDoesntCoverShipSize()
	{
		//given
		List<Point> toLongDeploymentPoints = new ArrayList<>( List.of( new Point( 1, 5 ), new Point( 2, 5 ), new Point( 3, 5 ), new Point( 4, 5 ) ) );

		//when
		assertThrows( IllegalArgumentException.class, () -> testPutOnBoard( shipWithSize3, toLongDeploymentPoints ) );

		//then
		assertNull( boardManager.getShipByPoint( new Point( 1, 5 ) ) );
		assertNull( boardManager.getShipByPoint( new Point( 2, 5 ) ) );
		assertNull( boardManager.getShipByPoint( new Point( 3, 5 ) ) );
		assertNull( boardManager.getShipByPoint( new Point( 4, 5 ) ) );
	}

	@Test
	void shouldNotAddShipWhenFieldOccupied()
	{
		//given
		testPutOnBoard( shipWithSize1, new ArrayList<>( List.of( new Point( 0, 0 ) ) ) );
		Ship ship2 = ShipFactory.create( ShipType.TEST_SHIP );

		//when
		ArrayList<Point> points = new ArrayList<>( List.of( new Point( 0, 0 ) ) );
		assertThrows( IllegalArgumentException.class, () -> testPutOnBoard( ship2, points ) );

		//then
		Ship shipFromBoard = boardManager.getShipByPoint( BoardManager.getPoint( 0, 0 ) );
		assertEquals( this.shipWithSize1, shipFromBoard );
	}

	@Test
	void shouldNotAddShipWhenMapEndDetected()
	{
		// S - Ship to be deployed
		// | - Board border
		// x - empty slot
		//    6 7 8 9|10
		//  7 x x x x|
		//  8 x x S S|S
		//  9 x x x x|

		//given
		List<Point> deploymentPoints = new ArrayList<>( List.of( new Point( 8, 8 ), new Point( 9, 8 ), new Point( 10, 8 ) ) );


		//when
		assertThrows( IllegalArgumentException.class, () -> testPutOnBoard( shipWithSize3, deploymentPoints ) );

		//then
		assertNull( boardManager.getShipByPoint( new Point( 10, 9 ) ) );
		assertNull( boardManager.getShipByPoint( new Point( 10, 10 ) ) );
		assertNull( boardManager.getShipByPoint( new Point( 10, 11 ) ) );
	}

	@Test
	void shouldNotAddShipAndNotChangeExistingShipLocationWhenCollisionDetected()
	{
		// S - Already deployed ship
		// D - Ship to be deployed
		// |C - Collision
		// x - empty slot
		//    6 7   8 9
		//  7 x S   x x
		//  8 D S|C D x
		//  9 x S   x x

		//given
		List<Point> correctDeploymentPoints = new ArrayList<>( List.of( new Point( 7, 7 ), new Point( 7, 8 ), new Point( 7, 9 ) ) );
		Ship alreadyDeployedShip = ShipFactory.create( ShipType.TEST_SHIP_WITH_SIZE_3 );
		boardManager.putOnBoard( correctDeploymentPoints, alreadyDeployedShip, PlacementType.VERTICAL );
		List<Point> collisionDeploymentPoints = new ArrayList<>( List.of( new Point( 6, 8 ), new Point( 7, 8 ), new Point( 8, 8 ) ) );
		//when
		assertThrows( IllegalArgumentException.class, () -> testPutOnBoard( shipWithSize3, collisionDeploymentPoints ) );

		//then
		assertNull( boardManager.getShipByPoint( new Point( 6, 8 ) ) );
		assertNull( boardManager.getShipByPoint( new Point( 8, 8 ) ) );
		assertEquals( alreadyDeployedShip, boardManager.getShipByPoint( new Point( 7, 7 ) ) );
		assertEquals( alreadyDeployedShip, boardManager.getShipByPoint( new Point( 7, 8 ) ) );
		assertEquals( alreadyDeployedShip, boardManager.getShipByPoint( new Point( 7, 9 ) ) );

	}

	@Test
	void shouldReturnAllPointWhereShipIsDeployed()
	{
		//given
		List<Point> correctDeploymentPoints = new ArrayList<>( List.of( new Point( 0, 0 ), new Point( 1, 0 ), new Point( 2, 0 ) ) );
		testPutOnBoard( shipWithSize3, correctDeploymentPoints );

		//when
		List<Point> pointToCheck = boardManager.getPointByShip( shipWithSize3 );
		correctDeploymentPoints.sort( Comparator.comparingInt( Point::getX ) );
		pointToCheck.sort( Comparator.comparingInt( Point::getX ) );

		//then
		assertEquals( correctDeploymentPoints, pointToCheck );
	}
}