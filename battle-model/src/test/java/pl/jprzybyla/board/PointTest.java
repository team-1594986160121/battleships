package pl.jprzybyla.board;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PointTest
{
	@Test
	void shouldCheckIfTwoPointsAreEqual()
	{
		Point pointA = new Point( 1, 1 );
		Point pointB = new Point( 1, 1 );
		Point pointC = new Point( 1, 2 );

		assertEquals( pointA, pointB );
		assertEquals( pointB, pointA );
		assertNotSame( pointA, pointB );
		assertNotSame( pointB, pointA );

		assertNotEquals( pointA, pointC );
		assertNotEquals( pointB, pointC );
	}
}