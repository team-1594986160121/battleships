package pl.jprzybyla.board.placement;

import org.junit.jupiter.api.Test;
import pl.jprzybyla.board.LineContinuityChecker;
import pl.jprzybyla.board.Point;
import pl.jprzybyla.placement.PlacementType;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static pl.jprzybyla.board.BoardManager.getPoint;

class LineContinuityCheckerTest
{
	@Test
	void shouldReturnTrueWhenLineIsContinuous()
	{
		//given
		LineContinuityChecker lineContinuityChecker = new LineContinuityChecker();
		List<Point> horizontalDeploymentPoints = new ArrayList<>( List.of( getPoint( 1, 5 ), getPoint( 2, 5 ), getPoint( 3, 5 ) ) );
		List<Point> verticalDeploymentPoints = new ArrayList<>( List.of( getPoint( 1, 5 ), getPoint( 1, 6 ), getPoint( 3, 7 ) ) );

		//when
		boolean horizontalCheck = lineContinuityChecker.checkDeploymentLineContinuous( horizontalDeploymentPoints, PlacementType.HORIZONTAL );
		boolean verticalCheck = lineContinuityChecker.checkDeploymentLineContinuous( verticalDeploymentPoints, PlacementType.VERTICAL );

		//then
		assertTrue( horizontalCheck );
		assertTrue( verticalCheck );

	}

	@Test
	void shouldThrowExceptionWhenLineIsNotContinuous()
	{
		//given
		LineContinuityChecker lineContinuityChecker = new LineContinuityChecker();
		List<Point> horizontalDeploymentPoints = new ArrayList<>( List.of( getPoint( 1, 5 ), getPoint( 1, 6 ), getPoint( 3, 7 ) ) );
		List<Point> verticalDeploymentPoints = new ArrayList<>( List.of( getPoint( 1, 5 ), getPoint( 2, 5 ), getPoint( 3, 5 ) ) );

		//when
		boolean horizontalCheck = lineContinuityChecker.checkDeploymentLineContinuous( horizontalDeploymentPoints, PlacementType.HORIZONTAL );
		boolean verticalCheck = lineContinuityChecker.checkDeploymentLineContinuous( verticalDeploymentPoints, PlacementType.VERTICAL );

		//then
		assertFalse( horizontalCheck );
		assertFalse( verticalCheck );
	}


	@Test
	void shouldCheckContinuityCorrectlyWhenPointsAreUnordered()
	{
		//given
		LineContinuityChecker lineContinuityChecker = new LineContinuityChecker();
		List<Point> horizontalDeploymentPoints = new ArrayList<>( List.of( getPoint( 3, 5 ), getPoint( 1, 5 ), getPoint( 2, 5 ) ) );
		List<Point> verticalDeploymentPoints = new ArrayList<>( List.of( getPoint( 1, 7 ), getPoint( 1, 5 ), getPoint( 3, 7 ) ) );

		//when
		boolean horizontalCheck = lineContinuityChecker.checkDeploymentLineContinuous( horizontalDeploymentPoints, PlacementType.HORIZONTAL );
		boolean verticalCheck = lineContinuityChecker.checkDeploymentLineContinuous( verticalDeploymentPoints, PlacementType.VERTICAL );

		//then
		assertTrue( horizontalCheck );
		assertFalse( verticalCheck );
	}
}