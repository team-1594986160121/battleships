package pl.jprzybyla.ships;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShipTest
{
	@Test
	void shouldApplyDamageCorrectly()
	{
		//given
		Ship ship = ShipFactory.create( ShipType.TEST_SHIP_WITH_SIZE_3 );

		//when
		ship.applyDamage( 2 );

		//then
		assertEquals( 1, ship.getCurrentHp() );
	}
}