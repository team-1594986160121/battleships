package pl.jprzybyla.ships;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static pl.jprzybyla.ships.ShipType.TOO_BIG_SHIP;

class ShipFactoryTest
{
	@Test
	void shouldThrowExceptionWhenShipSizeIsInvalid()
	{
		assertThrows( IllegalArgumentException.class, () ->
		{
			Ship tooBigShip = ShipFactory.create( TOO_BIG_SHIP );
		} );
	}
}