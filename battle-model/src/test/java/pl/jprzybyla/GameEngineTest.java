package pl.jprzybyla;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.jprzybyla.board.BoardManager;
import pl.jprzybyla.board.Point;
import pl.jprzybyla.placement.PlacementType;
import pl.jprzybyla.ships.Ship;
import pl.jprzybyla.ships.ShipFactory;
import pl.jprzybyla.ships.ShipType;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static pl.jprzybyla.board.BoardManager.getPoint;

class GameEngineTest
{
	private Ship ship;

	@BeforeEach
	void init()
	{
		ship = ShipFactory.create( ShipType.TEST_SHIP );;
	}

	@Test
	void shouldPositionShipsCorrectly()
	{
		//given
		Ship ship1 = ShipFactory.create( ShipType.TEST_SHIP_WITH_SIZE_3 );
		Ship ship2 = ShipFactory.create( ShipType.TEST_SHIP );
		Ship ship3 = ShipFactory.create( ShipType.TEST_SHIP_WITH_SIZE_3 );
		List<Ship> shipList = new ArrayList<>( List.of( ship1, ship2, ship3 ) );

		//when
		GameEngine gameEngine = new GameEngine( shipList );
		gameEngine.initSample();

		//then
		assertEquals( 7, gameEngine.getBoardManager()
		                           .getBoardPointsWithShips()
		                           .size() );

	}

	@Test
	void shouldDecreaseShipHpBy1WhenHit()
	{
		//given
		Ship ship = ShipFactory.create( ShipType.TEST_SHIP_WITH_SIZE_3 );
		GameEngine gameEngine = new GameEngine( new ArrayList<>( List.of( ship ) ) );
		gameEngine.initSample();
		List<Point> targetPoint = gameEngine.getBoardManager()
		                                    .getPointByShip( ship );

		//when
		gameEngine.shoot( targetPoint.get( 0 )
		                             .getX(), targetPoint.get( 0 )
		                                                 .getY() );

		//then
		assertEquals( 2, ship.getCurrentHp() );
	}

	@Test
	void shouldCallNotifyObserversAfterDestroyAllShips()
	{
		//given
		Ship ship1 = ShipFactory.create( ShipType.TEST_SHIP );
		Ship ship2 = ShipFactory.create( ShipType.TEST_SHIP );
		List<Ship> shipList = new ArrayList<>( List.of( ship1, ship2 ) );
		EventHandler eventHandler = spy( new EventHandler() );
		GameEngine gameEngine = new GameEngine( shipList, eventHandler );
		gameEngine.initSample();

		List<Point> target1 = gameEngine.getBoardManager()
		                                .getPointByShip( ship1 );
		List<Point> target2 = gameEngine.getBoardManager()
		                                .getPointByShip( ship2 );

		//when
		gameEngine.shoot( target1.get( 0 )
		                         .getX(), target1.get( 0 )
		                                         .getY() );
		gameEngine.shoot( target2.get( 0 )
		                         .getX(), target2.get( 0 )
		                                         .getY() );

		//then
		assertEquals( 0, ship1.getCurrentHp() );
		assertEquals( 0, ship2.getCurrentHp() );
		assertEquals( 0, gameEngine.getShips()
		                           .size() );
		verify( eventHandler, times( 1 ) ).notifyEndOfGame();
	}

	@Test
	void shouldCallNotifyObserversAfterHit()
	{
		//given
		EventHandler eventHandler = spy( new EventHandler() );
		GameEngine gameEngine = new GameEngine( new ArrayList<>( List.of( ship ) ), eventHandler );
		gameEngine.initSample();
		List<Point> target1 = gameEngine.getBoardManager()
		                                .getPointByShip( ship );
		//when
		gameEngine.shoot( target1.get( 0 )
		                         .getX(), target1.get( 0 )
		                                         .getY() );

		//then
		assertEquals( 0, ship.getCurrentHp() );
		verify( eventHandler, times( 1 ) ).notifyHit( target1.get( 0 ) );
	}

	@Test
	void shouldCallNotifyObserversAfterMiss()
	{
		//given
		BoardManager boardManager = new BoardManager();
		boardManager.putOnBoard( new ArrayList<>( List.of( getPoint( 0, 1 ) ) ), ship, PlacementType.VERTICAL );
		EventHandler eventHandler = spy( new EventHandler() );
		GameEngine gameEngine = new GameEngine( new ArrayList<>( List.of( ship ) ), eventHandler, boardManager );
		Point missedTarget = getPoint( 1, 1 );
		//when
		gameEngine.shoot( 1, 1 );

		//then
		verify( eventHandler, times( 1 ) ).notifyMiss( missedTarget );
	}

	@Test
	void shouldCallNotifyObserversAfterSunk()
	{
		//given
		EventHandler eventHandler = spy( new EventHandler() );
		GameEngine gameEngine = new GameEngine( new ArrayList<>( List.of( ship ) ), eventHandler );
		gameEngine.initSample();
		List<Point> target1 = gameEngine.getBoardManager()
		                                .getPointByShip( ship );
		//when
		gameEngine.shoot( target1.get( 0 )
		                         .getX(), target1.get( 0 )
		                                         .getY() );

		//then
		assertEquals( 0, ship.getCurrentHp() );
		verify( eventHandler, times( 1 ) ).notifySunk( List.of( target1.get( 0 ) ) );
	}
}