package pl.jprzybyla.placement;

import pl.jprzybyla.board.Point;

import java.security.SecureRandom;
import java.util.List;

public abstract class AbstractPlacementStrategy
{
	protected final SecureRandom rand;

	protected AbstractPlacementStrategy( SecureRandom aRand )
	{
		rand = aRand;
	}

	public abstract List<Point> createDeploymentLine( int aShipSize );

	public abstract PlacementType getPlacementType();
}
