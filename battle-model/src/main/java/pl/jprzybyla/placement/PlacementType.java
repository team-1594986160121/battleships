package pl.jprzybyla.placement;

import java.security.SecureRandom;

public enum PlacementType
{
	HORIZONTAL, VERTICAL;

	private static final SecureRandom random = new SecureRandom();

	public static <T extends Enum<?>> T randomPlacementType( Class<T> clazz )
	{
		int randomizedIndex = random.nextInt( clazz.getEnumConstants().length );
		return clazz.getEnumConstants()[randomizedIndex];
	}
}
