package pl.jprzybyla.placement;

import static pl.jprzybyla.ConstantValues.INVALID_PLACEMENT_TYPE;
import static pl.jprzybyla.ConstantValues.UTILITY_CLASS;

public class PlacementStrategyFactory
{
	private PlacementStrategyFactory()
	{
		throw new IllegalStateException( UTILITY_CLASS );
	}

	public static AbstractPlacementStrategy create( PlacementType aPlacementType )
	{
		switch ( aPlacementType )
		{
			case HORIZONTAL:
				return new HorizontalPlacementStrategy();
			case VERTICAL:
				return new VerticalPlacementStrategy();
			default:
				throw new IllegalArgumentException( INVALID_PLACEMENT_TYPE );
		}
	}
}
