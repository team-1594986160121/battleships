package pl.jprzybyla.placement;

import pl.jprzybyla.board.BoardManager;
import pl.jprzybyla.board.Point;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import static pl.jprzybyla.BoardSize.BOARD_HEIGHT;
import static pl.jprzybyla.BoardSize.BOARD_WIDTH;

class VerticalPlacementStrategy extends AbstractPlacementStrategy
{
	VerticalPlacementStrategy()
	{
		this( new SecureRandom() );
	}

	VerticalPlacementStrategy( SecureRandom aRand )
	{
		super( aRand );
	}

	@Override
	public List<Point> createDeploymentLine( int aShipSize )
	{
		Point startPoint = calculateStartPoint( aShipSize );
		List<Point> deploymentPoints = new ArrayList<>( List.of( startPoint ) );
		return addNextDeployPoints( aShipSize, deploymentPoints );
	}

	private Point calculateStartPoint( int aShipSize )
	{
		int x = rand.nextInt( BOARD_WIDTH.getValue() );
		int y = rand.nextInt( BOARD_HEIGHT.getValue() - aShipSize + 1 );
		return BoardManager.getPoint( x, y );
	}

	private List<Point> addNextDeployPoints( int aShipSize, List<Point> aDeploymentPoints )
	{
		for ( int j = 0; j < aShipSize - 1; j++ )
		{
			aDeploymentPoints.add( BoardManager.getPoint( aDeploymentPoints.get( j )
			                                                               .getX(), aDeploymentPoints.get( j )
			                                                                                         .getY() + 1 ) );
		}
		return aDeploymentPoints;
	}

	@Override
	public PlacementType getPlacementType()
	{
		return PlacementType.VERTICAL;
	}
}
