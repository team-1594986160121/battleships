package pl.jprzybyla;

import pl.jprzybyla.board.Point;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import static pl.jprzybyla.Events.END_OF_GAME;
import static pl.jprzybyla.Events.HIT;
import static pl.jprzybyla.Events.MISSED;
import static pl.jprzybyla.Events.SUNK;

class EventHandler
{
	private final PropertyChangeSupport observerSupport;

	EventHandler()
	{
		observerSupport = new PropertyChangeSupport( this );
	}

	void notifyObservers( PropertyChangeEvent aPropertyChangeEvent )
	{
		observerSupport.firePropertyChange( aPropertyChangeEvent );
	}

	public void registerObserver( String aEventType, PropertyChangeListener aObs )
	{
		observerSupport.addPropertyChangeListener( aEventType, aObs );
	}

	void notifyEndOfGame()
	{
		notifyObservers( new PropertyChangeEvent( this, END_OF_GAME.name(), null, null ) );
	}

	void notifyMiss( Point aPoint )
	{
		notifyObservers( ( new PropertyChangeEvent( this, MISSED.name(), null, makeList( aPoint ) ) ) );
	}

	void notifyHit( Point aPoint )
	{
		notifyObservers( ( new PropertyChangeEvent( this, HIT.name(), null, makeList( aPoint ) ) ) );
	}

	void notifySunk( List<Point> aPoints )
	{
		notifyObservers( ( new PropertyChangeEvent( this, SUNK.name(), null, aPoints ) ) );
	}

	private List<Point> makeList( Point aPoint )
	{
		return new ArrayList<>( List.of( aPoint ) );
	}
}
