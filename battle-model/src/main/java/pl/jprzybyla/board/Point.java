package pl.jprzybyla.board;

import java.util.Objects;

public class Point
{
	private final int x;
	private final int y;

	Point( int aX, int aY )
	{
		x = aX;
		y = aY;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	@Override
	public boolean equals( Object aO )
	{
		if ( this == aO )
		{
			return true;
		}
		if ( aO == null || getClass() != aO.getClass() )
		{
			return false;
		}
		Point point = ( Point ) aO;
		return x == point.getX() && y == point.getY();
	}

	@Override
	public int hashCode()
	{
		return Objects.hash( x, y );
	}
}
