package pl.jprzybyla.board;

import pl.jprzybyla.placement.PlacementType;
import pl.jprzybyla.ships.Ship;

import java.util.ArrayList;
import java.util.List;

import static pl.jprzybyla.ConstantValues.DEPLOYMENT_LINE_NOT_CONTINOUS;
import static pl.jprzybyla.ConstantValues.UNCOVERED_DEPLOYMENT_LINE;

public class BoardManager
{
	private final Board board = new Board();

	public void putOnBoard( List<Point> aDeploymentPoints, Ship aShip, PlacementType aPlacementType )
	{
		checkLineLength( aDeploymentPoints, aShip );
		checkLineContinuity( aDeploymentPoints, aPlacementType );
		board.add( aDeploymentPoints, aShip );
	}

	private void checkLineContinuity( List<Point> aDeploymentPoints, PlacementType aPlacementType )
	{
		LineContinuityChecker lineContinuityChecker = new LineContinuityChecker();
		if ( !lineContinuityChecker.checkDeploymentLineContinuous( aDeploymentPoints, aPlacementType ) )
		{
			throw new IllegalArgumentException( DEPLOYMENT_LINE_NOT_CONTINOUS );
		}
	}

	private void checkLineLength( List<Point> aDeploymentPoints, Ship aShip )
	{
		if ( aShip.getSize() != aDeploymentPoints.size() )
		{
			throw new IllegalArgumentException( UNCOVERED_DEPLOYMENT_LINE );
		}
	}

	public static Point getPoint( int aX, int aY )
	{
		return new Point( aX, aY );
	}

	public Ship getShipByPoint( Point aPoint )
	{
		return board.getShip( aPoint );
	}

	public List<Point> getBoardPointsWithShips()
	{
		return new ArrayList<>( board.getBoardPointsWithShips() );
	}

	public List<Point> getPointByShip( Ship aShip )
	{
		return board.getPoints( aShip );
	}
}
