package pl.jprzybyla.board;

import pl.jprzybyla.BoardSize;
import pl.jprzybyla.ships.Ship;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static pl.jprzybyla.ConstantValues.OUTSIDE_MAP;
import static pl.jprzybyla.ConstantValues.TILE_TAKEN;

class Board
{
	private final Map<Point, Ship> map;

	Board()
	{
		map = new HashMap<>();
	}

	Ship getShip( Point aPoint )
	{
		return map.get( aPoint );
	}

	List<Point> getPoints( Ship aShip )
	{
		return map.keySet()
		          .stream()
		          .filter( p -> map.get( p )
		                           .equals( aShip ) )
		          .collect( Collectors.toList() );
	}

	void add( List<Point> aDeploymentPoints, Ship aShip )
	{
		aDeploymentPoints.forEach( this::isDestinationAvailable );
		aDeploymentPoints.forEach( p -> map.put( p, aShip ) );
	}


	void isDestinationAvailable( Point aPoint )
	{
		throwExceptionIfTileIsTaken( aPoint );
		throwExceptionWhenIsOutsideMap( aPoint );
	}

	private void throwExceptionWhenIsOutsideMap( Point aPoint )
	{
		if ( aPoint.getX() < 0 || aPoint.getX() >= BoardSize.BOARD_WIDTH.getValue() || aPoint.getY() < 0 || aPoint.getY() >= BoardSize.BOARD_HEIGHT.getValue() )
		{
			throw new IllegalArgumentException( OUTSIDE_MAP );
		}
	}

	private void throwExceptionIfTileIsTaken( Point aPoint )
	{
		if ( isTileTaken( aPoint ) )
		{
			throw new IllegalArgumentException( TILE_TAKEN );
		}
	}

	private boolean isTileTaken( Point aPoint )
	{
		return map.containsKey( aPoint );
	}

	Set<Point> getBoardPointsWithShips()
	{
		return map.keySet();
	}
}
