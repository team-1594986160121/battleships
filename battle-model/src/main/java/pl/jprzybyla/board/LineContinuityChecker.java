package pl.jprzybyla.board;

import pl.jprzybyla.placement.PlacementType;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class LineContinuityChecker
{

	public boolean checkDeploymentLineContinuous( List<Point> aDeploymentPoints, PlacementType aPlacementType )
	{
		if ( aDeploymentPoints.isEmpty() )
		{
			return false;
		}

		if ( aPlacementType.equals( PlacementType.VERTICAL ) )
		{
			return innerContinuityCheck( aDeploymentPoints, this::getPointY );
		}
		else if ( aPlacementType.equals( PlacementType.HORIZONTAL ) )
		{
			return innerContinuityCheck( aDeploymentPoints, this::getPointX );
		}

		return false;
	}

	private boolean innerContinuityCheck( List<Point> aDeploymentPoints, Function<Point, Integer> aGetCoordinate )
	{
		aDeploymentPoints.sort( Comparator.comparingInt( aGetCoordinate::apply ) );

		for ( int i = 0; i < aDeploymentPoints.size() - 1; i++ )
		{
			if ( ( aGetCoordinate.apply( aDeploymentPoints.get( i + 1 ) ) - aGetCoordinate.apply( aDeploymentPoints.get( i ) ) ) != 1 )
			{
				return false;
			}
		}
		return true;
	}

	private int getPointX( Point aPoint )
	{
		return aPoint.getX();
	}

	private int getPointY( Point aPoint )
	{
		return aPoint.getY();
	}
}
