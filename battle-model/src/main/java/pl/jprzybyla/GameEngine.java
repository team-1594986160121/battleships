package pl.jprzybyla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.jprzybyla.board.BoardManager;
import pl.jprzybyla.board.Point;
import pl.jprzybyla.placement.AbstractPlacementStrategy;
import pl.jprzybyla.placement.PlacementStrategyFactory;
import pl.jprzybyla.placement.PlacementType;
import pl.jprzybyla.ships.Ship;

import java.beans.PropertyChangeListener;
import java.util.List;

import static pl.jprzybyla.ColumnCoordinatesSymbols.getColumnCoordinates;
import static pl.jprzybyla.ConstantValues.*;
import static pl.jprzybyla.placement.PlacementType.randomPlacementType;

public class GameEngine
{
	private final List<Ship> ships;
	private final BoardManager boardManager;
	private final EventHandler eventHandler;
	Logger log = LoggerFactory.getLogger( GameEngine.class );

	public GameEngine( List<Ship> aShips )
	{
		this( aShips, new EventHandler() );
	}

	GameEngine( List<Ship> aShips, EventHandler aEventHandler )
	{
		this( aShips, aEventHandler, new BoardManager() );

	}

	GameEngine( List<Ship> aShips, EventHandler aEventHandler, BoardManager aBoardManager )
	{
		ships = aShips;
		boardManager = aBoardManager;
		eventHandler = aEventHandler;
	}

	public void initSample()
	{
		log = LoggerFactory.getLogger( GameEngine.class );

		for ( Ship ship : ships )
		{
			PlacementType placementType = randomPlacementType( PlacementType.class );
			AbstractPlacementStrategy placementStrategy = PlacementStrategyFactory.create( placementType );
			tryToPutOnBoard( ship, placementStrategy );
		}
		if ( boardManager.getBoardPointsWithShips()
		                 .isEmpty() )
		{
			throw new IllegalStateException( INIT_FAILED );
		}
	}

	private void tryToPutOnBoard( Ship ship, AbstractPlacementStrategy aPlacementStrategy )
	{
		int counter = 0;
		int maxTries = 3;
		while ( counter != maxTries )
		{
			try
			{
				log.info( FIRST_POSITION_ATTEMPT );
				putShipOnBoard( ship, aPlacementStrategy );
				log.info( SUCCESSFUL_POSITION_ATTEMPT );
				break;
			}
			catch ( IllegalArgumentException aE )
			{
				log.info( NEXT_POSITION_ATTEMPT );
				++counter;
			}
		}
	}

	private void putShipOnBoard( Ship aShip, AbstractPlacementStrategy aPlacementStrategy )
	{
		List<Point> deploymentPoints = aPlacementStrategy.createDeploymentLine( aShip.getSize() );
		boardManager.putOnBoard( deploymentPoints, aShip, aPlacementStrategy.getPlacementType() );
	}

	BoardManager getBoardManager()
	{
		return boardManager;
	}

	List<Ship> getShips()
	{
		return ships;
	}

	public void registerObserver( String aEventType, PropertyChangeListener aObs )
	{
		eventHandler.registerObserver( aEventType, aObs );
	}

	public void shoot( int aX, int aY )
	{
		shoot( BoardManager.getPoint( aX, aY ) );
		String shotMessage = String.format( "%s  %s %s", SHOT_ON_POINT, getColumnCoordinates().get( aX ), ( aY + 1 ) );
		log.info( shotMessage );
	}

	private void shoot( Point aPoint )
	{
		Ship ship = boardManager.getShipByPoint( aPoint );
		if ( ship != null )
		{
			ship.applyDamage( DEFAULT_DAMAGE );
			log.info( HIT );
			eventHandler.notifyHit( aPoint );
			isDestroyed( ship );
			isEndOfGame();
		}
		else
		{
			log.info( MISSED );
			eventHandler.notifyMiss( aPoint );
		}
	}

	private void isDestroyed( Ship aShip )
	{
		if ( aShip.getCurrentHp() == 0 )
		{
			String destroyMessage = String.format( "%s  %s", aShip.getName(), DESTROYED );
			log.info( destroyMessage );
			ships.remove( aShip );
			List<Point> points = boardManager.getPointByShip( aShip );
			eventHandler.notifySunk( points );
		}
	}

	private void isEndOfGame()
	{
		if ( ships.isEmpty() )
		{
			eventHandler.notifyEndOfGame();
		}
	}

}
