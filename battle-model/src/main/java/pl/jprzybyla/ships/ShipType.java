package pl.jprzybyla.ships;

public enum ShipType
{
	DESTROYER( "Destroyer", 4 ), BATTLESHIP( "Battleship", 5 ), TEST_SHIP( "Test", 1 ), TEST_SHIP_WITH_SIZE_3( "Test_3", 3 ), TOO_BIG_SHIP("Too big", 40);

	private final String name;
	private final int size;

	ShipType( String aName, int aSize )
	{
		name = aName;
		size = aSize;
	}

	String getName()
	{
		return name;
	}

	int getSize()
	{
		return size;
	}
}
