package pl.jprzybyla.ships;

public class Ship
{
	private int currentHp;
	private final ShipType shipType;

	Ship( ShipType aShipType )
	{
		shipType = aShipType;
		currentHp = aShipType.getSize();
	}

	public int getSize()
	{
		return shipType.getSize();
	}

	public int getCurrentHp()
	{
		return currentHp;
	}

	public void applyDamage( int aDamageToDeal )
	{
		currentHp = currentHp - aDamageToDeal;
	}

	public String getName()
	{
		return shipType.getName();
	}
}
