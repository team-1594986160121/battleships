package pl.jprzybyla.ships;

import static pl.jprzybyla.ConstantValues.*;

public class ShipFactory
{
	private ShipFactory()
	{
		throw new IllegalStateException( UTILITY_CLASS );
	}

	public static Ship create( ShipType aShipType )
	{
		if ( 0 < aShipType.getSize() && aShipType.getSize() <= 10 )
		{

			switch ( aShipType )
			{
				case DESTROYER:
				case BATTLESHIP:
				case TEST_SHIP:
				case TEST_SHIP_WITH_SIZE_3:
				case TOO_BIG_SHIP:
					return new Ship( aShipType );
				default:
					throw new IllegalArgumentException( INVALID_SHIP_TYPE );
			}
		}
		else
		{
			throw new IllegalArgumentException( INVALID_SHIP_SIZE );
		}

	}
}
