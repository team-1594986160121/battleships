package pl.jprzybyla;

import java.util.ArrayList;
import java.util.List;

import static pl.jprzybyla.ConstantValues.UTILITY_CLASS;

public class ColumnCoordinatesSymbols
{
	private static final List<String> columnCoordinates = new ArrayList<>( List.of( "A", "B", "C", "D", "E", "F", "G", "H", "J", "K" ) );

	private ColumnCoordinatesSymbols()
	{
		throw new IllegalStateException( UTILITY_CLASS );
	}

	public static List<String> getColumnCoordinates()
	{
		return columnCoordinates;
	}
}
