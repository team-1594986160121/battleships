package pl.jprzybyla;

public class ConstantValues
{
	public static final String INVALID_SHIP_TYPE = "Invalid ship type";
	public static final String INVALID_SHIP_SIZE = "Invalid ship size";
	public static final String INVALID_PLACEMENT_TYPE = "Invalid placement type";
	public static final String FIRST_POSITION_ATTEMPT = "Attempting to position ships on the board.";
	public static final String NEXT_POSITION_ATTEMPT = "The positioning of the ship was not successful. Processes another attempt.";
	public static final String SUCCESSFUL_POSITION_ATTEMPT = "The ship has been set up successfully.";
	public static final String INIT_FAILED = "Failed to initialise board correctly";
	public static final String SHOT_ON_POINT = "Shot on point: ";
	public static final String HIT = "Hit!";
	public static final String MISSED = "Missed!";
	public static final String DESTROYED = " destroyed!";
	public static final String DEPLOYMENT_LINE_NOT_CONTINOUS = "The deployment line is not continuous";
	public static final String UNCOVERED_DEPLOYMENT_LINE = "Deployment line does not cover ship size";
	public static final String TILE_TAKEN = "Tile isn't empty";
	public static final String OUTSIDE_MAP = "You are trying to works outside the map";
	public static final String DIALOG_TITLE = "Endgame";
	public static final String DIALOG_TEXT_CONTENT = "All ships have been destroyed";
	public static final String OK = "Ok";
	public static final String MAIN_CSS = "main.css";
	public static final String GRID_CSS = "gridPane";
	public static final String UTILITY_CLASS = "Utility class";
	public static  final String IMAGE_ID = "state";
	public static  final String IMAGE_EXTENSION = ".png";
	public static  final String GRAPHIC_PATH = "/graphics/";
	public static  final String HASH = "#";
	public static final int DEFAULT_DAMAGE = 1;

	private ConstantValues()
	{
		throw new IllegalStateException( UTILITY_CLASS );
	}
}
