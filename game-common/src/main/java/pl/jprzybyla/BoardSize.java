package pl.jprzybyla;

public enum BoardSize
{
	BOARD_WIDTH( 10 ), BOARD_HEIGHT( 10 );

	private final int value;

	BoardSize( int aValue )
	{
		value = aValue;
	}

	public int getValue()
	{
		return value;
	}
}
