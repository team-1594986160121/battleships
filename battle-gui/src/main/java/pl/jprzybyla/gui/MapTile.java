package pl.jprzybyla.gui;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import static pl.jprzybyla.ConstantValues.*;

class MapTile extends StackPane
{
	public static final int MAP_TILE_SIZE = 70;
	private final Rectangle rec;

	MapTile()
	{
		rec = new Rectangle( MAP_TILE_SIZE, MAP_TILE_SIZE );
		rec.setStroke( Color.BLACK );
		rec.setFill( Color.WHITE );
		getChildren().add( rec );
	}

	void setIndex( String aIndex )
	{
		Label label = new Label( aIndex );
		getChildren().add( label );
		StackPane.setAlignment( label, Pos.TOP_LEFT );
	}

	void changeTileState( String aState )
	{
		setDisable( true );
		clearPrevious();
		ImageView image = new ImageView( new Image( getClass().getResourceAsStream( GRAPHIC_PATH + aState + IMAGE_EXTENSION ) ) );
		image.setFitHeight( 46 );
		image.setFitWidth( 46 );
		image.setId( IMAGE_ID );
		getChildren().add( image );
	}

	private void clearPrevious()
	{
		if ( lookup( HASH + IMAGE_ID ) != null )
		{
			getChildren().remove( lookup( HASH + IMAGE_ID ) );
		}
	}
}
