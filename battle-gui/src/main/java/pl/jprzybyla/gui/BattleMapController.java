package pl.jprzybyla.gui;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import pl.jprzybyla.BoardSize;
import pl.jprzybyla.GameEngine;
import pl.jprzybyla.board.Point;
import pl.jprzybyla.ships.Ship;
import pl.jprzybyla.ships.ShipFactory;
import pl.jprzybyla.ships.ShipType;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import static pl.jprzybyla.ColumnCoordinatesSymbols.getColumnCoordinates;
import static pl.jprzybyla.ConstantValues.*;
import static pl.jprzybyla.Events.*;
import static pl.jprzybyla.Events.HIT;
import static pl.jprzybyla.Events.MISSED;

class BattleMapController implements PropertyChangeListener
{
	private final GameEngine gameEngine;
	private BorderPane rootPane;
	private GridPane gridMap;

	BattleMapController()
	{
		gameEngine = new GameEngine( prepareSampleDeployment() );
		initGameSample();
		gameEngine.registerObserver( HIT.name(), this );
		gameEngine.registerObserver( MISSED.name(), this );
		gameEngine.registerObserver( END_OF_GAME.name(), this );
		gameEngine.registerObserver( SUNK.name(), this );
		initGui();
	}

	private void initGameSample()
	{
		try
		{
			gameEngine.initSample();
		}
		catch ( IllegalStateException aE )
		{
			createEndGameDialog( aE.getMessage() );
		}
	}

	private List<Ship> prepareSampleDeployment()
	{
		Ship ship1 = ShipFactory.create( ShipType.BATTLESHIP );
		Ship ship2 = ShipFactory.create( ShipType.DESTROYER );
		Ship ship3 = ShipFactory.create( ShipType.DESTROYER );

		return new ArrayList<>( List.of( ship1, ship2, ship3 ) );
	}

	private void initGui()
	{
		rootPane = new BorderPane();
		rootPane.getStylesheets()
		        .add( MAIN_CSS );
		gridMap = new GridPane();
		gridMap.getStyleClass()
		       .add( GRID_CSS );

		for ( int x = 0; x < BoardSize.BOARD_WIDTH.getValue(); x++ )
		{
			for ( int y = 0; y < BoardSize.BOARD_HEIGHT.getValue(); y++ )
			{
				final int coordinateX = x;
				final int coordinateY = y;

				MapTile rec = new MapTile();
				rec.setIndex( getColumnCoordinates().get( x ) + ( y + 1 ) );
				rec.addEventHandler( MouseEvent.MOUSE_CLICKED, e -> gameEngine.shoot( coordinateX, coordinateY ) );
				gridMap.add( rec, x, y );
			}
		}

		rootPane.setCenter( gridMap );
	}

	BorderPane getRootPane()
	{
		return rootPane;
	}

	@Override
	public void propertyChange( PropertyChangeEvent aPropertyChangeEvent )
	{
		if ( aPropertyChangeEvent.getPropertyName()
		                         .equals( END_OF_GAME.name() ) )
		{
			createEndGameDialog( DIALOG_TEXT_CONTENT );
		}
		else
		{
			List<?> point = ( List<?> ) aPropertyChangeEvent.getNewValue();

			point.forEach( p ->
			{
				MapTile tile = getNodeFromGridMap( ( Point ) p );
				if ( tile != null )
				{
					tile.changeTileState( aPropertyChangeEvent.getPropertyName() );
				}
			} );
		}
	}

	private void createEndGameDialog( String aMessage )
	{
		Dialog<String> dialog = new Dialog<>();
		dialog.setTitle( DIALOG_TITLE );
		dialog.setContentText( aMessage );
		ButtonType type = new ButtonType( OK, ButtonBar.ButtonData.OK_DONE );
		dialog.getDialogPane()
		      .getButtonTypes()
		      .add( type );
		dialog.showAndWait();
		Platform.exit();
	}

	private MapTile getNodeFromGridMap( Point aPoint )
	{
		for ( Node node : gridMap.getChildren() )
		{
			if ( GridPane.getColumnIndex( node ) == aPoint.getX() && GridPane.getRowIndex( node ) == aPoint.getY() && node instanceof MapTile )
			{
				return ( MapTile ) node;
			}
		}
		return null;
	}
}
