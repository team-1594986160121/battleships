package pl.jprzybyla.gui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class BattleStart extends Application
{

	@Override
	public void start( Stage aStage )
	{
		BattleMapController battleMapController = new BattleMapController();
		Scene scene = new Scene( battleMapController.getRootPane(), 950, 850 );
		aStage.setScene( scene );
		aStage.centerOnScreen();
		aStage.setOnCloseRequest( e -> Platform.exit() );
		aStage.show();
	}

	public static void main( String[] args )
	{
		launch();
	}
}